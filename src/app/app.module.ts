import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidenavComponent } from './sidenav/sidenav.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { WinterComponent } from './winter/winter.component';
import { HttpClientModule } from "@angular/common/http";
import { HouseComponent } from './house/house.component';
import { DecorationComponent } from './decoration/decoration.component';
import { TradingComponent } from './trading/trading.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { NgImageSliderModule} from 'ng-image-slider';
import { HomeService } from "./home/home.service";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    WinterComponent,
    SidenavComponent,
    WinterComponent,
    HouseComponent,
    DecorationComponent,
    TradingComponent,
    AboutusComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    // * MATERIAL IMPORTS
    MatSidenavModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    HttpClientModule,
    NgImageSliderModule,
  ],
  providers: [HomeService,],
  bootstrap: [AppComponent],
})
export class AppModule {}
