import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WinterComponent } from './winter/winter.component'
import { HouseComponent } from './house/house.component'
import { DecorationComponent } from './decoration/decoration.component'
import { TradingComponent } from './trading/trading.component'
import { AboutusComponent } from './aboutus/aboutus.component'
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'winter', component: WinterComponent },
  { path: 'house', component: HouseComponent },
  { path: 'decoration', component: DecorationComponent },
  { path: 'trading', component: TradingComponent },
  { path: 'aboutus', component: AboutusComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
