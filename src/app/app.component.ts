import { Component } from '@angular/core';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ){
    this.matIconRegistry.addSvgIcon(
      "winter",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/snow.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "decoration",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/decoration.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "trade",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/trade.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "garden",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/garden.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "aboutus",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/aboutus.svg")
    );
  }

  title = '4-elements';
  sideBarOpen = false;

  sideBarToggler() {
    this.sideBarOpen = !this.sideBarOpen;
  }
}
