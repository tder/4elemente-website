import { Injectable, Inject, Optional } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class HomeService {
  constructor(private http: HttpClient) { }

  data: any =
    [{
      image: '../../assets/home/home1.jpg',
      thumbImage: '../../assets/home/home1.jpg',
      alt: 'Home',
      title: 'Home'
    }, {
      image: '../../assets/home/home2.jpg',
      thumbImage: '../../assets/home/home2.jpg',
      alt: 'Home',
      title: 'Home'
    }, {
      image: '../../assets/home/home3.jpg',
      thumbImage: '../../assets/home/home3.jpg',
      alt: 'Home',
      title: 'Home'
    }, {
      image: '../../assets/home/home4.jpeg', // Support base64 image
      thumbImage: '../../assets/home/home4.jpeg', // Support base64 image
      title: 'Home', //Optional: You can use this key if want to show image with title
      alt: 'Home', //Optional: You can use this key if want to show image with alt
      order: 1 //Optional: if you pass this key then slider images will be arrange according @input: slideOrderType
    }
    ];
  getImages() {
    return this.http.get(
      "https://accedo-video-app-api.herokuapp.com/getImages",
      httpOptions
    );
  }

  getImagesWithOrder() {
    return this.data;
  }
}
